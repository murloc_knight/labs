#include <stdio.h>
#include <math.h>

#define PI 3.1415926535

struct trig
{
	double sin1;
	double sinn;

	double cos1;
	double cosn;
};

int nn = 2;

void iter(int n,struct trig* t ,double* r){
	if (n < nn)
	{
		*r += t->sinn / (n*n*n);

		for (int i = 0; i < 2; ++i)
		{
			double ts = t->sinn * t->cos1 + t->cosn * t->sin1;
			double tc = t->cosn * t->cos1 - t->sinn * t->sin1;

			t->sinn = ts;
			t->cosn = tc;
		}
		iter(n+2,t,r);
	}
}

double perfect(double x){
	return x * (PI - x)*(PI/8);
}

int main()
{

	//double x = 1;

	//scanf("%lf",&x);

/*	double sin1 = sin(x);
	double cos1 = cos(x);
	double r = 0;
	struct trig t = {sin1,sin1,cos1,cos1};*/

	for (double x = 1.0; x <= 2; x+=0.01)
	{
			double sin1 = sin(x);
			double cos1 = cos(x);
			double r = 0;
			struct trig t = {sin1,sin1,cos1,cos1};
			iter(1,&t,&r);
			printf("%lf\n", r);
	}

	return 0;
}