#include <stdio.h>
#include <malloc.h>
#include <mem.h>

int str_len(char* s);

int main()
{
	char text[20] = {0};

	printf("write the text>");
	fgets(text,sizeof(text),stdin);
	printf("\n");

	int len = str_len(text);

	char* buff = malloc(len);

	int j = 0;
	for (int i = len - 2; i >= -1 ; --i)
	{
		if (' ' == text[i] || i == -1)
		{
			memcpy(text + i + 1,buff,j);
			j = 0;
			for (int n = 0; n < len; ++n)
			{
				buff[n] = 0;
			}
			continue;
		}
		buff[j++] = text[i];
	}
	printf("\n");
	printf("%s\n", text);

	free(buff);
	return 0;
}

int str_len(char* s){
	int l = 0;
	while(*s){
		l++;
		s++;
	}
	return l;
}