// ConsoleApplication40.cpp: определяет точку входа для консольного приложения.
//

#include <Windows.h>
#include <math.h>
#include <stdio.h>

char EXIT[] = "exit";

void lb13 ();
void lb14 ();
void lb15 ();
void lb16 ();
void lb17 ();
void lb18 ();
void lb19 ();
void lb20 ();
void lb21 ();
void lb22 ();

int getNumLen(int n){
	return (n == 0) ? 1 : ceil(log10((float) abs(n) + 0.5f));
}

int str_equal(char* a,char* b){
	while (*a && *b)
	{
		if (*a != *b)
		{
			return 0;
		}
		a++;
		b++;
	}
	return *a == *b;
}

int main()
{

	char command[10];
	for (int i = 0; i < 10; i++)
	{
		command[i] == 0;
	}

	while (1)
	{
		printf("enter the program number or help for reference>>");
		scanf("%s",&command);
		if (str_equal(command,EXIT))
		{
			break;
			return 0;
		}else if (str_equal(command,"help")){
			printf("1 sum of digits in number\n2 search for the largest number in a number\n3 inverts a number\n4 write a number to the array character by characte\n5 search for numbers which is equal to the cube of the sum of numbers in it\n6 search for palindrome numbers\n7 remove negative array elements\n8 exclude the largest element\n9 and 10: find primes in a sequence\nexit for close program\n");
		}else if (str_equal(command,"1")){
			lb13();
		}else if (str_equal(command,"2")){
			lb14();
		}else if (str_equal(command,"3")){
			lb15();
		}else if (str_equal(command,"4")){
			lb16();
		}else if (str_equal(command,"5")){
			lb17();
		}else if (str_equal(command,"6")){
			lb18();
		}else if (str_equal(command,"7")){
			lb19();
		}else if (str_equal(command,"8")){
			lb20();
		}else if (str_equal(command,"9")){
			lb21();
		}else if (str_equal(command,"10")){
			lb22();
		}
	}

	return 0;
}

//sum of digits in number
void lb13(){
	printf("enter a number to calculate the sum of the digits in it>>");
	int a;
	scanf("%u",&a);

	if (a == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int n = 0;
	int k = 0;
	int s = 0;
	for (n=a, s=0; n!=0; n=n/10){
		k=n%10; s=s+k;
	}
	printf("%d\n",s);
}

//search for the largest number in a number
void lb14(){
	printf("enter a number to find the highest digits in it>>");
	int a;
	scanf("%u",&a);
	if (a == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int n = 0;
	int s = 0;
	int k = 0;
	for (n=a, s=0; n!=0; n=n/10)
      { k=n%10; if (k>s) s=k;}

	printf("%d\n",s);
}

//inverts a number
void lb15(){
	printf("enter the number to invert>>");
	int a;
	scanf("%u",&a);
	if (a == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}


	int n = 0;
	int s = 0;
	int k = 0;

	for (n=a, s=0; n!=0; n=n/10)
      { k=n%10; s=s*10+k;}

	printf("%d\n",s);
}

//write a number to the array character by character
void lb16(){
	printf("see the number to write it as an array>>");
	int a;
	scanf("%u",&a);
	if (a == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int i = 0;
	int n = 0;

	int* A = malloc(getNumLen(a) * 4);

	for (i=0, n=a; n!=0; i++, n=n/10);
		for (A[i--]=-1, n=a; n!=0; i--, n=n/10)
			A[i]=n % 10; 

	for(int i = 0; i < getNumLen(a); i++){
		printf("%d ",A[i]);
	}
	printf("\n");
	//delete A;
}

//search for numbers which is equal to the cube of the sum of numbers in it
void lb17(){
	int j = 0;
	int s = 0;
	int k = 0;
	int n = 0;
	int a = 0;

	int A[30000];

	for (j=0,a=10; a<30000; a++){ 
      	for (n=a, s=0; n!=0; n=n/10){
			k=n%10; s=s+k;
		}
		if (a==s*s*s)
			A[j++]=a;
	}

	printf("a list of numbers less than 30,000 cube of the sum in which is equal to the number itself:");
	for(int i = 0; i < j; i++){
		printf("%d ",A[i]);
	}

	printf("\n");
}

//search for palindrome numbers
void lb18(){
	printf("enter the upper border of the palindrome number search>>");
	int v;
	scanf("%u",&v);
	if (v == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int j = 0;
	int a = 0;
	int n = 0;
	int s = 0;
	int k = 0;

	int* A = malloc(v * 4);

	for (j=0,a=10; a<v; a++){ 
      	for (n=a, s=0; n!=0; n=n/10){
			k=n%10; s=s*10+k;
		}
		if (a==s) A[j++]=a;
	}

	for(int i = 0; i < j;i++){
		printf("%d ",A[i]);
	}
	printf("\n");
}

//remove negative array elements
void lb19(){
	int i = 0;
	int j = 0;

	printf("enter the number of elements in the array from which you want to remove negative elements>>");
	int n;
	scanf("%u",&n);
	if (n == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	printf("enter an array:\n");

	int* A = malloc(n * 4);
	for (int i = 0; i < n; i++)
	{
		printf(">>");
		scanf("%d", &A[i]);
		if (A[i] == -858993460)
		{
			A[i] == 0;
			i--;
		}
	}


	for (i=0; i<n; i++){
		if (A[i]<0){
			for (j=i; j<n-1;j++)
				A[j]=A[j+1];
			n--;
			i--;
		}
	}

	for (int i = 0; i < n; i++)
	{
		printf("%d ", A[i]);
	}

	printf("\n");
}

//exclude the largest element
void lb20(){
	int i = 0;
	int j = 0;
	int k = 0;
	printf("enter the number of elements in the array from which you want to remove the largest element>>");
	int n;
	scanf("%u",&n);
	if (n == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	printf("enter an array:\n");

	int* A = malloc(n * 4);
	for (int i = 0; i < n; i++)
	{
		printf(">>");
		scanf("%d", &A[i]);
		if (A[i] == -858993460)
		{
			A[i] == 0;
			i--;
		}
	}

	for (i=1,k=0; i<n; i++)
		if (A[i]>A[k]) 
			k=i;

	for (j=k; j<n-1;j++)
		A[j]=A[j+1];
	n--;

	for (int i = 0; i < n; i++)
	{
		printf("%d ", A[i]);
	}

	printf("\n");
}

//find primes in a sequence
void lb21(){
	int i = 0;
	int a = 0;
	int n = 0;

	printf("enter the top border of the prime number search>>");
	int v;
	scanf("%u",&v);
	if (v == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int* A = malloc(v * 4);

	for (i=0,a=2; a<v; a++){
      	for (n=2; n<a; n++){
			if (a%n==0)
				break; 
		}
      	if (n==a)
			A[i++]=a;
    }
	A[i]=0;

	for (i = 0; A[i] != 0; i++)
	{
		printf("%d ", A[i]);
	}

	printf("\n");
}

void lb22(){
	int i = 0;
	int a = 0;
	int s = 0;
	int n = 0;

	printf("enter the top border of the prime number search>>");
	int v;
	scanf("%u",&v);
	if (v == -858993460)
	{
		printf("Invalid number format\n");
		return;
	}

	int* A = malloc(v * 4);

	for (i=0,a=2; a<v; a++){
      	for (s=0,n=2; n<a; n++)
           	if (a%n==0){
				s=1;
				break; 
			}
      	if (s==0) A[i++]=a;
      	}
	A[i]=0;

	for (int i = 0; A[i] != 0; i++)
	{
		printf("%d ", A[i]);
	}

	printf("\n");
}